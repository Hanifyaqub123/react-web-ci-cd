const { assert } = require("chai")

describe('React application home page', ()=>{
    it("Verify that the applinks says Learn React", ()=>{
        browser.url('/')

        let text = $(".App-link").getText()
        assert.equal(text, 'Learn React')
    })
})